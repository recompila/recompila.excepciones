﻿' Para utilizar este módulo es necesario realizar los siguientes ajustes en la solución/proyecto.
'   
'   - AÑADIR MANEJADOR EN ApplicationEvents PARA CAPTURAR LAS UnhandledExceptions.
'       - Ver proyecto GestorExcepciones >> Pruebas
'
'   - AÑADIR UN FORMULARIO PARA LA RECOGIDA DE DATOS DEL CLIENTE
'       - Se puede utilizar el que ya se encuentra en Excepciones\Formularios, sino
'         se puede crear uno propio
'
'   - Se debe crear en Sistema.Excepciones los valores de configuración del sistema de Excepciones
'       - Ver proyecto GestorExcepciones >> Pruebas
'
'   - Se pueden añadir más datos a las excepciones capturadas dependiendo del programa y lo que se quiera
'     capturar, para ello en el objeto OtrosDatos se debe incluir el objeto que se quiere enviar con la excepción.
'     Este objeto tiene que poder ser serializable y no tener valores Nothings par aque no se produzcan errores.
'     El visor de excepciones debe ser capaz de recomponer este objeto

Imports System.IO
Imports System.Management
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Recompila.Helper

Namespace Excepciones

    ''' <summary>
    ''' Clase utilizada para recopilar la información que se guardará de las excepciones producidas en el programa. 
    ''' Se pueden añadir mas propiedades, simplemente se añaden en el objeto OtrosDatos (el cual tiene que ser serializable)
    ''' y en el momento de producir la excepción, rellenarlos y añadirlos a la esta clase.
    ''' </summary>
    Public Class cInformeExcepcion
#Region " PROPIEDADES "
        ' Datos relativos a la persona que estaba ejecutando el programa (contacto con esta)
        Public Property concedeContacto As Boolean = False
        Public Property emailContacto As String = String.Empty
        Public Property telefonoContacto As String = String.Empty

        ' Datos relativos a la excepción
        Public Property capturarPantalla As Boolean = True
        Public Property captura As Object = Nothing
        Public Property comentario As String = String.Empty
        Public Property DirectoryPath As String = String.Empty
        Public Property LoadedAssemblies As String = String.Empty
        Public Property StackTrace As String = String.Empty
        Public Property WorkingSet As Long = 0
        Public Property ipLocal As String = String.Empty
        Public Property nombreEquipoLocal As String = String.Empty
        Public Property nombreUsuarioLocal As String = String.Empty

        ' Otros campos personalizados 
        Public Property campoPersonalizado1 As String = String.Empty
        Public Property campoPersonalizado2 As String = String.Empty
        Public Property campoPersonalizado3 As String = String.Empty
        Public Property campoPersonalizado4 As String = String.Empty

        ' Otros datos que se van a incluir con la excepción 
        Public Property OtrosDatos As Object = Nothing
#End Region

#Region " CONSTRUCTORES "
        Public Sub New()
            ' Se recoge la información de la aplicación, uso de memoria, stracktrace...
            Try
                Me.DirectoryPath = My.Application.Info.DirectoryPath
                For Each unEnsamblado As System.Reflection.Assembly In My.Application.Info.LoadedAssemblies
                    LoadedAssemblies &= unEnsamblado.FullName & "|" & vbCrLf
                Next
                Me.StackTrace = My.Application.Info.StackTrace
                Me.WorkingSet = My.Application.Info.WorkingSet
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al obtener información de una excepción no controlada.", ex, New StackTrace(0, True))
            End Try

            ' Se recoge información del usuario actualmente conectado y
            ' información del sistema
            Try
                Me.nombreEquipoLocal = System.Net.Dns.GetHostName
                For Each unaIP As Net.IPAddress In System.Net.Dns.GetHostAddresses(Me.nombreEquipoLocal)
                    Me.ipLocal &= unaIP.ToString & " | "
                Next
                Me.nombreUsuarioLocal = System.Environment.UserName
            Catch ex As Exception
                If Log._LOG_ACTIVO Then Log.escribirLog("ERROR al obtener información del usuario conectado en una excepción no controlada.", ex, New StackTrace(0, True))
            End Try
        End Sub
#End Region
    End Class







    Public Module modExcepciones
        ''' <summary>
        ''' Envía un informe de error al servicio web
        ''' </summary>
        ''' <param name="eExcepcion">Excepción que se produce y fue capturada</param>
        ''' <param name="eURL_Servicio_WEB">URL del servicio WEB</param>
        ''' <param name="eNombre_Servicio_WEB">Nombre del servicio WEB</param>
        ''' <param name="eMetodo_Servicio_WEB">Metodo a invocar en el servicio web</param>
        ''' <param name="eCodigoPrograma">Código identificativo del programa</param>
        ''' <param name="eInforme">Informe con los datos de la excepción</param>
        ''' <param name="eEsperarRespuesta">Ejecución en modo sincrono o asincrono</param>
        ''' <returns>En modo Sincrono, True si todo funcionó correctamente, en caso contrario False</returns>
        Public Function EnviarError(ByVal eExcepcion As Exception, _
                                    ByVal eURL_Servicio_WEB As String, _
                                    ByVal eNombre_Servicio_WEB As String, _
                                    ByVal eMetodo_Servicio_WEB As String, _
                                    ByVal eCodigoPrograma As String, _
                                    Optional ByVal eInforme As cInformeExcepcion = Nothing, _
                                    Optional eEsperarRespuesta As Boolean = False) As Boolean

            ' Se comprueba que hay conexión a Internet, en caso contrario, no es posible
            ' reportar el informe con la excepción
            If Not My.Computer.Network.IsAvailable Then Return False

            ' Se preparan todos los parámetros para enviar al servicio Web de
            ' recogida de excepciones
            Try
                Dim Parametros As New List(Of Object)

                ' Si no se adjunta un informe de error, este se crea
                If eInforme Is Nothing Then eInforme = New cInformeExcepcion

                ' Serialización de la excepción y los datos de StackTrace y Description de la excepción capturada
                Dim ExcepcionSerializada As String = ""
                Dim ExcepcionTexto As String = ""

                If eExcepcion IsNot Nothing Then
                    Using Datos As MemoryStream = Serializacion.serializarObjeto2Stream(eExcepcion)
                        ExcepcionSerializada = System.Convert.ToBase64String(Datos.ToArray())
                        Datos.Close()
                    End Using
                    ExcepcionTexto = eExcepcion.Message & vbCrLf & eExcepcion.StackTrace
                Else
                    ExcepcionSerializada = System.Convert.ToBase64String(New Byte() {})
                    ExcepcionTexto = "-Sin excepción-"
                End If

                ' Si hay que capturar la pantalla, pero esta todavía no se capturo, se
                ' realiza la captura en este momento. Independientemente de la captura, se serializa
                ' antes de enviar la excepción al servidor
                Dim CapturaSerializada As String = ""
                If eInforme.capturarPantalla Then
                    If eInforme.captura Is Nothing Then
                        CapturaSerializada = Imagenes.Imagen2Base64(Imagenes.CapturarImagenPantalla)
                    Else
                        CapturaSerializada = Imagenes.Imagen2Base64(eInforme.captura)
                    End If
                Else
                    CapturaSerializada = System.Convert.ToBase64String(New Byte() {})
                End If

                ' Se serializa el objeto con el resto de datos extras que se envían con la excepción
                Dim otrosDatosSerializado As String = ""
                If eInforme.OtrosDatos IsNot Nothing Then
                    Using Datos As MemoryStream = Serializacion.serializarObjeto2Stream(eInforme.OtrosDatos)
                        otrosDatosSerializado = System.Convert.ToBase64String(Datos.ToArray())
                        Datos.Close()
                    End Using
                Else
                    otrosDatosSerializado = System.Convert.ToBase64String(New Byte() {})
                End If


                ' Se preparan todos los parámetros que se le van a pasar al servicio Web
                ' para poder almacenar la excepción en el servidor de excepciones no controladas
                With Parametros
                    '   Información del Sistema Operativo
                    .Add(My.Computer.Info.OSFullName.ToString)          ' Sistema Operativo
                    .Add(My.Computer.Info.OSVersion.ToString)           ' Version Sistema Operativo
                    .Add(SistemasOperativos.getOSPlatForm.ToString)     ' Plataforma Sistema Operativo

                    '   Información de la aplicación
                    .Add(My.Application.Info.AssemblyName.ToString)     ' Nombre del ensamblado
                    .Add(My.Application.Info.ProductName.ToString)      ' Nombre del producto
                    .Add(eCodigoPrograma.ToString)                      ' Código interno del programa
                    .Add(My.Application.Info.Version.ToString)          ' Versión del ensamblado en la que se produce el error
                    .Add(eInforme.DirectoryPath.ToString)               ' Ruta desde donde se inició la aplicación
                    .Add(eInforme.LoadedAssemblies.ToString)            ' Ensamblados y versiones utilizdaas
                    .Add(eInforme.StackTrace.ToString)                  ' Pila de llamadas y estado actual del programa
                    .Add(eInforme.WorkingSet.ToString)                  ' Total memoria asignada para el Workingset

                    '   Información relativa al equipo/usuario
                    .Add(eInforme.ipLocal.ToString)                     ' IP del equipo 
                    .Add(eInforme.nombreEquipoLocal.ToString)           ' Nombre del equipo
                    .Add(eInforme.nombreUsuarioLocal.ToString)          ' Nombre del usuario                    

                    '   Información de la excepción
                    .Add(ExcepcionSerializada)                 ' Excepcion serializado
                    .Add(ExcepcionTexto)                       ' Descripción de la excepción + StackTrace
                    .Add(eInforme.comentario)                  ' Comentario y/o aclaración del cliente
                    .Add(CapturaSerializada)                   ' Captura d epantalla serializada

                    '   Campos personalizados para información extra
                    .Add(eInforme.campoPersonalizado1)         ' Campo personalizado 1 
                    .Add(eInforme.campoPersonalizado2)         ' Campo personalizado 2 
                    .Add(eInforme.campoPersonalizado3)         ' Campo personalizado 3 
                    .Add(eInforme.campoPersonalizado4)         ' Campo personalizado 4 

                    '   Campos relativos al contacto
                    .Add(Funciones.booleanABit(eInforme.concedeContacto).ToString)             ' Permite contactar con el                     
                    .Add(eInforme.emailContacto)               ' Email de contacto
                    .Add(eInforme.telefonoContacto)            ' Teléfono de contacto

                    '   Datos extras incluidos con la excepción
                    .Add(otrosDatosSerializado)             ' Datos extras incluidos en la excepción

                    ' Se pueden añadir más parámetros, estos tienenen que ir incluidos en el objeto OtrosDatos del informe
                End With

                ' Se puede esperar a que se realice el envío de la excepción o lanzarla en un hilo
                If eEsperarRespuesta Then
                    Dim elResultado As Object = ServiciosWeb.CallWebService(eURL_Servicio_WEB, eNombre_Servicio_WEB, eMetodo_Servicio_WEB, Parametros.ToArray)
                    If elResultado IsNot Nothing Then
                        If Log._LOG_ACTIVO Then Log.escribirLog("Resultado WebService: " & elResultado.ToString, , New StackTrace(0, True))
                    End If
                Else
                    Dim Hilo As New Thread(AddressOf EnviarErrorInterno)
                    Hilo.Start(New Object() {eURL_Servicio_WEB, eNombre_Servicio_WEB, eMetodo_Servicio_WEB, Parametros})
                End If

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Envío de la excepción mediante un Hilo
        ''' </summary>
        ''' <param name="Datos">parametros con los que s eva a enviar la excepción</param>
        Public Sub EnviarErrorInterno(ByVal Datos)
            Dim Aux As List(Of Object) = Datos(3)
            ServiciosWeb.CallWebService(Datos(0), Datos(1), Datos(2), Aux.ToArray)
        End Sub
    End Module
End Namespace
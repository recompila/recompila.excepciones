﻿Namespace Excepciones
    Public Class cInformacionErrorNoControlado
        Public Property CapturarPantalla As Boolean
        Public Property ConcedeContacto As Boolean
        Public Property Personalizado1 As String
        Public Property Personalizado2 As String
        Public Property ComentarioCliente As String
        Public Property Licencia As String
        Public Property ImagenCaptura As Object
    End Class
End Namespace
